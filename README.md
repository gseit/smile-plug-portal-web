SMILE Plug Access Panel

This is a backbone app. To build the app, call jake build. The source files in /src will be compiled into the /target directory. For deployment only use the contents of the target directory.

Directory Structure:

- /src - Source files
- /target - Target files

Jake can be installed by npm and requires nodejs.

- git clone https://bitbucket.org/gseit/smile-plug-portal-web
- npm install -g jake
- npm install
- jake build

Make the changes first to the /src directory. After jake build, the /target directory files will be built and contain the modified changes.
