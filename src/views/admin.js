(function(app) {
    app.AdminView = app.View.extend({
        initialize: function() {
            //only create template once, but ensure that creation takes place after app is loaded
            if (!this.template) app.AdminView.prototype.template = this.template || _.template($('#admin-template').html());
        },

        events: {
            'click #plug-power-off': 'powerOff',
            'click #couchdb-compact': 'couchdbCompact'
        }, 

        render: function() {
            this.$el.html(this.template());
            logger.info("AdminView rendered");

            _this = this;

            // get ethernet IP if available
            jQuery.get("assets/get-eth0ip.php", function(data) {
                if (!data) 
                    return;
                
                _this.$el.find("#ethernet-ip").text(data.trim());
            });

            // get wifi mac if possible
            jQuery.get("assets/get-wifimac.php", function(data) {
                if (!data) 
                    return;

                _this.$el.find("#wifi-mac").text(data.trim());
            });

            jQuery.post( "/smileService/usage", { who: app.localStorage.getItem("who"), who_uuid: app.localStorage.getItem("who_uuid"), what: "opened", message: "the admin portal" } );
            return this;
        },

        isTeacher: function(e) {
            var userName = app.localStorage.getItem("who");
            
            if (userName && userName.toLowerCase().includes('teacher') === true) {
                return true; 
            }

            return false;
        },

        powerOff: function(e) {
            // check for teacher
            var proceed = this.isTeacher();
            var userName = (app.localStorage.getItem("who")) ?  app.localStorage.getItem("who") : 'blank';

            if (proceed) {
                // make the post to turn off plug
                jQuery.post("assets/service-goodnight.php");
            } else {
                window.alert('You must be a teacher to perform this task. If you are a teacher, add the word teacher to your name, which is currently ' + userName + '.');
            }

            return this;
        },

        couchdbCompact: function(E) {
            jQuery.post( "/couchdb/_compact", {} );
            return this;
        },

        remove: function() {
            logger.info("AdminView removed");
            this.parentRemove();
        }
    })
})(app);