(function(app) {

    app.UsageDataModel = Backbone.Model.extend({ url: '/couchdb/_design/usage/_view/all?descending=true&include_docs=true&stale=update_after' });

    app.AnalyticsView = app.View.extend({
        initialize: function() {
            //only create template once, but ensure that creation takes place after app is loaded
            if (!this.template) app.AnalyticsView.prototype.template = this.template || _.template($('#analytics-template').html());

            // fetch the data 
            this.usageData = new app.UsageDataModel();
            this.usageData.fetch(); 

            // on receipt, render the view
            this.listenTo(this.usageData,'change', this.render); 
        },

        'events': {
            'click .toggle-user': 'toggleUser',
            'keyup #user-filter': 'userFilter'
        },

        timeSince: function(date) {
            var seconds = Math.floor((new Date() - new Date(date)) / 1000);
            var interval = Math.floor(seconds / 31536000);

            if (interval >= 1) {
                return date;
            }
            interval = Math.floor(seconds / 2592000);
            if (interval >= 1) {
                return date;
            }
            interval = Math.floor(seconds / 86400);
            if (interval >= 1) {
                return interval + " day" + (interval !== 1 ? 's' : '' )+ " ago";
            }
            interval = Math.floor(seconds / 3600);
            if (interval >= 1) {
                return interval + " hour" + (interval !== 1 ? 's' : '' )+ " ago";
            }
            interval = Math.floor(seconds / 60);
            if (interval >= 1) {
                return interval + " minute" + (interval !== 1 ? 's' : '' )+ " ago";
            }
            interval = Math.floor(seconds);
            return interval + " second" + (interval !== 1 ? 's' : '' )+ " ago";
        },

        toggleUser: function (ev) {
            var uuid = $(ev.currentTarget).data('filter-uuid');

            // hide opacity for user ids
            $("#usage-log li.user").css("opacity", 0.2);
            $("#usage-log li.user." + uuid).css("opacity", 1);
        },

        render: function() {

            // Generate list of unique UUID from the data
            var sortedUsage = {};
            for (var index in this.usageData.attributes.rows) {
                var row = this.usageData.attributes.rows[index];

                // Create new array only if it doens't exist
                if (!(row.value.who_uuid in sortedUsage)) 
                    sortedUsage[row.value.who_uuid] = Array();

                // Add all rows to array
                sortedUsage[row.value.who_uuid].push(row);
            }

            // render this in the template
            this.$el.html(this.template({usageData: this.usageData.attributes.rows, timeSince: this.timeSince, users: this.users, sortedUsage: sortedUsage }));

            logger.info("AnalyticsView rendered");
            return this;
        },

        userFilter: function(e) {
            var string = e.target.value.toLowerCase();

            // hide all lines
            $("#usage-log li.user").hide();

            // loop through all lines
            $("#usage-log li.user").each(function(index) {
                // if includes search string, show
                if ($(this).data('filter-name').toLowerCase().includes(string)) {
                    $(this).show();
                }
            });
        },

        remove: function() {
            logger.info("AnalyticsView removed");
            // clearInterval(this.timer);
            this.parentRemove();
        }
    })

})(app);